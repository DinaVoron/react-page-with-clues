import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
//import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);



function CreateForm(props) {
  const [value, setValue] = React.useState("");
  const[extraValue, setExtraValue] = React.useState("");
  const[curValue, setCurValue] = React.useState("");
  const arr_ = props.list;

  const Send = (event) => {
    event.preventDefault();
    if (!arr_.includes(value + extraValue)) {
      console.log("length: " + arr_.length);
      console.log("We have a new value");
      arr_.push(value);
      console.log("A new value added");
      console.log("new length: " + arr_.length);
    }
    alert(value + extraValue); 
    setValue("");
    setExtraValue("");
    setCurValue("");
  }

  const Change = (event) => {
    let val = "";
    //добавим напечатанное значение (если у нас не было extra символов)
    if (extraValue === "") {
      val = event.target.value;
    }
    else { //если extra символы были
      
      let eventValue = event.target.value;
      if (eventValue.length - extraValue.length > value.length) { //был ввод
        val = value + eventValue.substr(eventValue.length - 1);
        console.log("User typed smth...");
      } else {
        val = value.substr(0,value.length - 1);
        console.log("User deleted smth...")
      }
    }
    
    setValue(val);
    console.log("Value: " + val);

    // setExtraValue("");
    let ev = "";

    //теперь узнаем, есть ли похожее значение в списке
    if (val !== "") {
      console.log("Searching...");
      for (let i = 0; i < arr_.length; i++) {
        if (arr_[i].indexOf(val) === 0) {
          console.log("Found... " + arr_[i]);
          ev = arr_[i].substr(val.length);
          break;
        }
      }
    }

    setCurValue(val + ev);
    setExtraValue(ev);
    //event.target.value = val + ev;
  }


  return (
    <div className="form">
      <form>
        <input placeholder="Начните вводить - мы подскажем!" size="30" type="text" onChange={Change} name="textInput" value={curValue}></input>
        <input type="submit" value="Отправить" onClick={Send}></input>
      </form>
    </div>
  );
};

const arrValue = ["red","green", "blue"];

let element = <CreateForm list={arrValue} />

root.render(element);
